package engineeringsoftwaresystems.com.prova14sss;


import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.bluetooth.le.BluetoothLeScanner;


import android.bluetooth.le.ScanCallback;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 **/

public class DeviceService extends Service
{
    public static String TAG = "B=B DeviceService";

    public static final String ACTION_NO_OP = "com.nordicsemi.ACTION_NO_OPERATION";

    public static final String ACTION_SELECT_DEVICE = "com.nordicsemi.ACTION_SELECT_DEVICE";

    public static final String DEVICE_DOES_NOT_SUPPORT_BLE = "com.nordicsemi.DEVICE_DOES_NOT_SUPPORT_BLE";


    private BluetoothAdapter mBluetoothAdapter;

    private List<BluetoothDevice> deviceList;
    private Map<String, Integer> devRssiValues;
    private static final long SCAN_PERIOD = 10 * 1000; /* 10 seconds */
    private Handler mHandler;
    private boolean mScanning;


    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;

    private BluetoothManager mBluetoothManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    @Override
    @SuppressLint("NewApi")
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate.start");




        mHandler = new Handler();

        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

        Log.d(TAG, "mBluetoothManager="+mBluetoothManager);


        if(mBluetoothManager!=null)
        {
            mBluetoothAdapter = mBluetoothManager.getAdapter();

        }

        Log.d(TAG, "mBluetoothAdapter ="+mBluetoothAdapter);


        if(mBluetoothAdapter == null)
        {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }

        Log.d(TAG, "mBluetoothAdapter ="+mBluetoothAdapter);


        // Check if Bluetooth is supported on the device
        if (mBluetoothAdapter == null) {
            sendBLENotSuppotedBroadcast();
            stopSelf();
            return;
        }

        Log.d(TAG,"BLE is Supported...");


        if (!mBluetoothAdapter.isEnabled()) {

            Log.d(TAG,"Enabling BT Adapter.");

            mBluetoothAdapter.enable();
        }

        Log.d(TAG,"mBluetoothAdapter.isEnabled="+mBluetoothAdapter.isEnabled());


         if (!isAndroidLessThanLollipop())
        {
            initScanCallback();

            Log.d(TAG, "init LE scanner");



            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters = new ArrayList<ScanFilter>();





       }

        Log.d(TAG, "mLEScanner ="+mLEScanner);

        Log.d(TAG, "onCreate.end");
    }


    public void sendBLENotSuppotedBroadcast()
    {
        Intent intent = new Intent();
        intent.setAction(DEVICE_DOES_NOT_SUPPORT_BLE);
        sendBroadcast(intent);
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG,"onStartCommand.begin");

        String action = ACTION_NO_OP;

        if(intent!=null) {
            action = intent.getAction();
        }

        Log.d(TAG,"action="+action);


        if(ACTION_SELECT_DEVICE.equals(action))
        {
            scanLeDevice(true);
        }

        Log.d(TAG,"onStartCommand.end");
        return super.onStartCommand(intent, flags, startId);
    }

    @SuppressLint("NewApi")
    private void scanLeDevice(final boolean enable) {
        try {
            if (enable) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAndroidLessThanLollipop()) {
                            try {
                                mBluetoothAdapter.stopLeScan(mLeScanCallback);
                            }
                            catch (Exception e)
                            {
                                Log.d(TAG,"scanLeDevice.mBluetoothAdapter.stopLeScan.exception e="+e.getMessage());
                            }
                        } else {
                           try{
                            mLEScanner.stopScan(mScanCallback);
                        }
                            catch (Exception e)
                        {
                            Log.d(TAG,"scanLeDevice.mLEScanner.stopScan.exception e="+e.getMessage());
                        }
                        }
                    }
                }, SCAN_PERIOD);
                if (isAndroidLessThanLollipop()) {
                    try{
                    mBluetoothAdapter.startLeScan(mLeScanCallback);
                    }
                    catch (Exception e)
                    {
                     Log.d(TAG,"scanLeDevice.mBluetoothAdapter.startLeScan.exception e="+e.getMessage());
                    }
                } else {
                    try {
                    mLEScanner.startScan(filters, settings, mScanCallback);
                }
                catch (Exception e)
                {
                    Log.d(TAG,"scanLeDevice.mLEScanner.startScan.exception e="+e.getMessage());
                }


                }
            } else {
                if (isAndroidLessThanLollipop()) {
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                } else {
                    mLEScanner.stopScan(mScanCallback);
                }
            }

        }
        catch (Exception e)
        {
            Log.d(TAG,"scanLeDevice exception e="+e.getMessage());

        }



    }

    private boolean isAndroidLessThanLollipop()
    {
        Log.d(TAG,"Build.VERSION.SDK_INT="+Build.VERSION.SDK_INT);

        boolean result =  Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP;

        Log.d(TAG,"isAndroidLessThanLollipop="+result);

        return result;
    }


    private ScanCallback mScanCallback = null;

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private void initScanCallback() {
        mScanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                Log.i("callbackType", String.valueOf(callbackType));
                Log.i("result", result.toString());
                BluetoothDevice btDevice = result.getDevice();
                connectToDevice(btDevice);
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                for (ScanResult sr : results) {
                    Log.i("ScanResult - Results", sr.toString());
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                Log.e("Scan Failed", "Error Code: " + errorCode);
            }
        };

    }


    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("onLeScan", device.toString());
                            connectToDevice(device);
                        }
                    }).start();
                }
            };


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy.begin");

        scanLeDevice(false);

        Log.d(TAG, "onDestroy.end");

    } /* onDestroy */


    public void connectToDevice(BluetoothDevice device) {


        Log.d(TAG, "connectToDevice().start");

        String deviceName = device.getName();

        Log.d(TAG, "connectToDevice : Device in range : " + device.getName());


            if (deviceName != null) {
                if (deviceName.equals("EW-MW010") ||
                        deviceName.equals("EW-TW-7968") ||
                        deviceName.equals("EW-TM-7962") ||
                        ((deviceName.length() > 7) && (deviceName.substring(0, 8).equals("MCF-6902"))))

                {

                    Log.d(TAG, "** This is a Fitness device : deviceName=" + deviceName);
                    Log.d(TAG, "** Device Address : " + device.getAddress());

                    Bundle b = new Bundle();
                    b.putString(BluetoothDevice.EXTRA_DEVICE, device.getAddress());
                    Intent result = new Intent();
                    result.putExtras(b);
                    result.setAction(ACTION_SELECT_DEVICE);
                    Log.d(TAG,"sendBroadcast ACTION_SELECT_DEVICE");
                    sendBroadcast(result);

                    scanLeDevice(false);

                }
            }


    }



}





