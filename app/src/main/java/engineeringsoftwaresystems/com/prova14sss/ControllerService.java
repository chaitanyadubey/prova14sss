package engineeringsoftwaresystems.com.prova14sss;

//
//  MyActivity.java
//
//  Created by Michael Palumbo on 9/19/16.
//  Copyright © 2016 Michael Palumbo,
//                   Engineering Software Systems All rights reserved.
//
/* ======================================================================== */
/*            Engineering Software Systems Proprietary                      */
/* This document/file contains proprietary information, and except with     */
/* written permission from Engineering Software Systems, such information   */
/* shall not be published or disclosed to others or used for any purpose    */
/* other than as agreed upon in writing.  This information shall not be     */
/* copied or stored in electronic format, in whole or in part, in source,   */
/* object or executable format.                                             */
/*                                                                          */
/* (C) Copyright 2016-2016 Engineering Software Systems,                    */
/*                            All Rights Reserved                           */
/* ======================================================================== */
/* Module : MyActivity.java                                                 */
/* Date:    October 01, 2016                                                */
/* Author:  Michael J. Palumbo                                              */
/* ======================================================================== */

/* Notes: to interact with this class follow the instructions below         */
/* step 1: create this class                                                */
/* step 2: call init();                                                     */
/* step 3: call myInitialize();                                             */
/* step 4: call myScan4BTDevices();                                         */
/* step 5: monitor if connection established -- if not getIsDisconnected    */
/* step 6: monitor if communication established - if getIsConnected         */
/* step 7: set program info as needed such as:                              */
/*          setEnglishMetric(true);                                         */
/*          setUserParameters(1, true, 50, true, 66, 150);                  */
/*          setUserWorkoutParameters(1, 12, 0, 0, 10, 0);                   */
/* step 8: setWorkoutStart();                                               */
/* step 9: to stop program - setWorkoutStop();                              */
/* step10: to disconnect - setDisconnect();                                 */
/* step11: after getIsDisconnected == true then                             */
/*            can go back to step 3 as needed                               */
/* ======================================================================== */


import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;

import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

import android.util.Log;

import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;



public class ControllerService extends Service {


    public String TAG =  "B=B ControllerService";

    private DeviceReceiver mDeviceReceiver;

    private final IBinder mBinder = new LocalBinder();

    private static final int REQUEST_SELECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_DOWNLOAD_PATH = 3;

    private BluetoothAdapter mBtAdapter;
    private UartService mUartService = null;
    private UARTStatusChangeReceiver mUARTStatusChangeReceiver;

    private String deviceAddress;

    private int sendCommand;

    private boolean b_sendenable;
    private boolean b_connected;

    private Timer timer;
    private BluetoothDevice mDevice;
    private int startDelay;


    private int debugCounter;


    /* program variables */
    private int stage;
    private int next_msg2send;
    private int iter_count;
    private int cur_level;
    private int prog_mode;   /* 0=manual, 1=auto program mode */
    private int c3_cmd;     /* 0=nothing, 1=start, 2=stop, 4=valid resistance step */
    private boolean req_stop; /* added 2016-12-31 */



    /* user settings */
    private int u_mode;     /* mode desired 0:manual, 1:preset program */
    private int u_timeGoal;
    private int u_distanceGoal;
    private int u_calorieGoal;
    private int u_wattGoal;
    private int u_targetHRC;
    private int u_program;
    private int u_user;
    private int u_language;
    private int u_age;
    private int u_sex;
    private int u_weight;
    private int u_height;
    private int u_englishMetric;

    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */

    /* values received from console */
    private int c_wattSupportEnabled;   /* from D1 command byte 3 */
    private int c_hrcSupportEnabled;    /* from D1 command byte 4 */
    private int c_maxLevel;             /* from D1 command byte 5 */
    private int c_maxSlope;             /* from D1 command byte 6 */
    private int c_metricEnglish;        /* from D1 command byte 7 */
    private int c_max_programs;         /* from D1 command byte 8 */
    private int c_modelCoding1;         /* from D1 command byte 13 */
    private int c_modelCoding2;         /* from D1 command byte 14 */

    private int c_current_user;         /* from D2 command byte 3 */
    private int c_current_language;     /* from D2 command byte 4 */
    private int c_current_age;          /* from D2 command byte 5 */
    private int c_current_sex;          /* from D2 command byte 6 */
    private int c_current_weightH;      /* from D2 command byte 7 - hi byte */
    private int c_current_weightL;      /* from D2 command byte 8 - lo byte */
    private int c_current_weight;
    private int c_current_heightH;      /* from D2 command byte 9 - hi byte */
    private int c_current_heightL;      /* from D2 command byte 10- lo byte */
    private int c_current_height;
    private int c_current_totalTimeH;   /* from D2 command byte 11 - ?? */
    private int c_current_totalTimeL;   /* from D2 command byte 12 - ?? */
    private int c_current_totalDistanceH;/*from D2 command byte 13 - ?? */
    private int c_current_totalDistanceL;/*from D2 command byte 14 - ?? */
    private int c_current_totalCaloriesH;/*from D2 command byte 15 - ?? */
    private int c_current_totalCaloriesL;/*from D2 command byte 16 - ?? */

    private int c_current_minutes;      /* from D3 command byte 3 */
    private int c_current_seconds;      /* from D3 command byte 4 */
    private int c_current_distance1_0;  /* from D3 command byte 5 : whole miles/km */
    private int c_current_distance0_1;  /* from D3 command byte 6 : tenths of miles/km */
    private int c_current_distance;
    private int c_current_caloriesH;    /* from D3 command byte 7 : calories hi byte */
    private int c_current_caloriesL;    /* from D3 command byte 8 : calories lo byte */
    private int c_current_calories;
    private int c_current_wattsH;       /* from D3 command byte 9 : hi byte */
    private int c_current_wattsL;       /* from D3 command byte 10: lo byte */
    private int c_current_watts;
    private int c_current_heartrate;    /* from D3 command byte 11 */
    private int c_current_speedH;       /* from D3 command byte 12 : hi byte */
    private int c_current_speedL;       /* from D3 command byte 13 : lo byte */
    private int c_current_speed;
    private int c_current_slope;        /* from D3 command byte 14 */
    private int c_current_level;        /* from D3 command byte 15 : 1-max */
    private int c_current_RPMx10_H;     /* from D3 command byte 16 : RPMx10 - hi byte */
    private int c_current_RPMx10_L;     /* from D3 command byte 17 : RPMx10 - lo byte */
    private int c_current_RPMx10;
    private int c_current_status;       /* from D3 command byte 18 bits 0,1 (00:stopped/end, 01:running, 10=pause) .. bit 2:(0=manual, 1=prog, 2=countdown) */

    private int c_current_mode;         /* from D4 command byte 3 (1=program */
    private int c_current_program;      /* from D4 commamd byte 4 */

    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */

    /* commanded values */

    private int w_resistance_level;
    private boolean w_new_resistance_level_commanded;
    private int w_workout_run_state;  /* 0=not running, 1=running, 2=paused */
    private int w_workout_mode;       /* 0=manual, 1=auto program */



    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */



    public class LocalBinder extends Binder {
        ControllerService getService() {
            return ControllerService.this;
        }
    }

    /*
Class DeviceReceiver
Description : This class is used to receive broadcast messages from DeviceService
 */
    public class DeviceReceiver extends BroadcastReceiver
    {
        public static final String TAG = "B=B DeviceReceiver";

        public String bundleToString(Bundle extras)
        {
            final Set<String> keySet = extras.keySet();
            StringBuffer sb = new StringBuffer();
            for (final String key: keySet) {
                sb.append('\"');
                sb.append(key);
                sb.append("\"=\"");
                sb.append(extras.get(key));
                sb.append("\", ");
            }
            return sb.toString();

        }
        @Override
        public void onReceive(Context context, Intent intent)
        {
           // Toast.makeText(context, "onReceive ", Toast.LENGTH_SHORT).show();
            Log.d(TAG,"onReceive");

            String action = intent.getAction();

            Log.d(TAG, "action="+action);

            if(DeviceService.ACTION_SELECT_DEVICE.equals(action)) {
                Bundle bundle = intent.getExtras();

                deviceAddress = bundle.getString(BluetoothDevice.EXTRA_DEVICE);
                mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);
                mUartService.connect(deviceAddress);
                Log.d(TAG, "connecting to..." + deviceAddress);
            }
            else
            if(DeviceService.DEVICE_DOES_NOT_SUPPORT_BLE.equals(action)) {

                Log.d(TAG, "BLE not suppoted");


            }

        }

    }//receiver inner class ends

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }




    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG,"onDestroy.start");

        stopServices();

        Log.d(TAG,"onDestroy.stop");

    }

    public void stopServices()
    {
        Log.d(TAG,"stopServices.start");

        try
        {
            unregisterReceiver(mUARTStatusChangeReceiver);
        }
        catch (Exception e)
        {
            Log.d(TAG,"stopServices.exception1 e="+e.getMessage());
        }

        try {
            unregisterReceiver(mDeviceReceiver);
        }
        catch (Exception e)
        {
            Log.d(TAG,"stopServices.exception2 e="+e.getMessage());
        }

        try {

            unbindService(mUartServiceConnection);
        }
        catch (Exception e)
        {
            Log.d(TAG,"stopServices.exception3 e="+e.getMessage());
        }

        try {
            Intent intent = new Intent();
            intent.setClass(this, DeviceService.class);
            stopService(intent);
        }
        catch (Exception e)
        {
            Log.d(TAG,"stopServices.exception4 e="+e.getMessage());
        }



        try
        {
            timer.cancel();// Terminates this timer, discarding any currently scheduled tasks.
            timer.purge();// Removes all cancelled tasks from this timer's task queue.
        }
        catch (Exception e)
        {
            Log.d(TAG,"stopServices.exception1 e="+e.getMessage());
        }

        try
        {
            stopSelf();
        }
        catch (Exception e)
        {
            Log.d(TAG,"stopServices.exception6 e="+e.getMessage());
        }



        Log.d(TAG,"stopServices.end");



    }




    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */
    /* |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| */

    private void initVars() {
        stage = 0;
        iter_count = 0;
        c_current_status = 0;
        req_stop = false;

        u_user = 1;
        u_mode = 1; /* preset program */
        u_language = 0;
        u_age = 50;
        u_sex = 1;
        u_weight = 150;
        u_height = 65;
        u_englishMetric = 0; /* english */
        u_calorieGoal = 0;
        u_distanceGoal = 0; /* x 100 */
        u_timeGoal = 5; /* minutes */
        u_targetHRC = 0;
        u_program = 13;

        cur_level = 0;
        prog_mode = 1; /* auto program */
        c3_cmd = 0;

        /* initialize console status variables */
        c_wattSupportEnabled = 0;
        c_hrcSupportEnabled = 0;
        c_metricEnglish = 0;
        c_maxLevel = 0;
        c_maxSlope = 0;
        c_current_program = 0;
        c_modelCoding1 = 0;
        c_modelCoding2 = 0;
        c_current_user = 0;
        c_current_language = 0;
        c_current_age = 0;
        c_current_sex = 0;
        c_current_weight = 0;
        c_current_weightH = 0;
        c_current_weightL = 0;
        c_current_heightH = 0;
        c_current_heightL = 0;
        c_current_height = 0;



    } /* initVars */

    public void myInitialize() {
        initVars();
    } /* myInitialize */


    // UART service connected/disconnected
    private ServiceConnection mUartServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName ClassName, IBinder rawBinder) {
            Log.d(TAG, "onServiceConnected");
            mUartService = ((UartService.LocalBinder) rawBinder).getService();
            if (!mUartService.initialize()) {
                finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName classname) {
            mUartService = null;
        }
    };


    @Override
    public void onCreate() {

        // removed debug 2016-11-15
        // enable this to show screen display
try {
  //  StartupClass sc = new StartupClass(this);
} catch (Exception e) {
            Log.d(TAG, e.getMessage());
    Log.d(TAG, (e.getCause() == null) ? "n/a" : e.getCause().toString());
        }
        Log.d(TAG, "onCreate2");
        startDelay = 0;
        debugCounter = 0;
        initVars();


        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            // draw little popup message
            Toast.makeText(this, "BT Not Supported", Toast.LENGTH_LONG).show();
            finish();
            return;
        } else {
//            Toast.makeText(this, "Got to this step", Toast.LENGTH_LONG).show();
        }

        initService();  // prepare to catch bluetooth events

//        if (false) {  // moved to separate functions
//            if (mBtAdapter.isEnabled()) {
//                /* do stuff here */
//                Log.i(TAG, "got to this step");
//                Toast.makeText(this, "Got to this step", Toast.LENGTH_LONG).show();
//
//                if (b_connected == false) {
//
//                    myScan4BTDevices();
//                }
//
//                if (true) {
//                    timer = new Timer();
//                    timer.schedule(new RunTimerTask(), 0, 500); // was 300
//                    // timer.schedule(new ProgramTask(), 0, 1000);
//                }
//            } // if mBtAdapter.isEnabled
//
//            b_sendenable = false;
//        } // if false
    }


    public void init() {
        timer = new Timer();
        timer.schedule(new RunTimerTask(), 0, 300); // was 300
        // timer.schedule(new ProgramTask(), 0, 1000);

    } // init

    public void myScan4BTDevices() {

        Log.d(TAG,"myScan4BTDevices.START b_connected="+b_connected);

        if (b_connected == false) {
            // sendCommand = 0;
//            Intent newIntent = new Intent(MyActivity.this, DeviceService.class);
//            startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);


            Log.d(TAG,"myScan4BTDevices starting DeviceService");

            Intent newIntent = new Intent();
            newIntent.setClass(this, DeviceService.class);
            newIntent.setAction(DeviceService.ACTION_SELECT_DEVICE);
            startService(newIntent);


        }

        Log.d(TAG,"myScan4BTDevices.END");

    } // myScan4BTDevices */

//Scan and Connect
    public void scanAndConnect()
    {
        Log.d(TAG, "button-sendMessage");
        myInitialize(); // init the vars
        init();   //init the timer
        initService();// register the broadcast listeners
        myScan4BTDevices();// scan for BT devices
    }

    public void startWorkOut() {
        Log.d(TAG, "******* Button2 startWorkOut");
        setEnglishMetric(true); /* false = metric */
        setUserParameters(1, true, 50, true, 66, 150);
        setUserWorkoutParameters(1, 12, 0, 0, 10, 0);
        setWorkoutStart();
    }

    public void stopWorkout()
    {
        Log.d(TAG, "******* Button3 stopWorkout");
        if (debugCounter == 0)
        {
            setWorkoutStop();
            debugCounter++;
        }
        else
        {
            setDisconnect();
            debugCounter = 0;
        }
    }

    /** Called when the user clicks the send button */
 //   public void sendMessage(View view) {
//Log.d("mybut","*************ButtonClicked");
//        setEnglishMetric(true); /* false = metric */
//        setUserParameters(1, true, 50, true, 66, 150);
//        setUserWorkoutParameters(1, 12, 0, 0, 10, 0);
//        setWorkoutStart();
//    }






//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Log.i(TAG, "MyActivity-onActivityResult");
//        switch (requestCode) {
//            case REQUEST_SELECT_DEVICE:
//                Log.i(TAG, "MyActivity-onActivityResult:REQUEST_SELECT_DEVICE");
//                if (resultCode == Activity.RESULT_OK && data != null) {
//                    deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
//                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);
//                    mUartService.connect(deviceAddress);
//                    Log.d(TAG, "connecting");
//                }
//                break;
//            case REQUEST_ENABLE_BT:
//                Log.i(TAG, "MyActivity-onActivityResult-REQUEST_ENABLE_BT");
//                if (requestCode == Activity.RESULT_OK) {
//                    Log.d(TAG, "bt_enable-ok");
//                }
//                else {
//                    Log.d(TAG, "enable-device ERROR");
//                    finish();
//                }
//                break;
//            case REQUEST_DOWNLOAD_PATH:
//                Log.i(TAG, "MyActivity-onActivityResult-REQ_DOWNLOAD_PATH");
//                break;
//        }
//    } /* onActivityResult */


    public void finish() {
        if (timer != null) {
            timer.purge();
            timer.cancel();
            timer = null;
        }
       stopSelf();
    }

    private void initService() {
        Log.d(TAG, "initService.start");

        try {



        Log.d(TAG, "register DeviceReceiver");
        mDeviceReceiver = new DeviceReceiver();
        IntentFilter intentFilter1 = new IntentFilter();
        intentFilter1.addAction(DeviceService.ACTION_SELECT_DEVICE);
        intentFilter1.addAction(DeviceService.DEVICE_DOES_NOT_SUPPORT_BLE);
        registerReceiver(mDeviceReceiver,intentFilter1);

        Log.d(TAG, "start DeviceService");


        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mUartServiceConnection, Context.BIND_AUTO_CREATE);

        mUARTStatusChangeReceiver = new UARTStatusChangeReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        Log.d(TAG, "initService/middle");

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mUARTStatusChangeReceiver, intentFilter);

        }
        catch (Exception e)
        {
            Log.d(TAG, "initService.exception e="+e.getMessage());
        }
        Log.d(TAG, "initService.end");
    }

    // Run at 1 Hz
    private class ProgramTask extends TimerTask {
        private int pt_debugCounter = 0;

        @Override
        public void run() {
        }
    } /* private class ProgramTask */


    // Run at 3.33 Hz
    private class RunTimerTask extends TimerTask {
        private int rt_debugCounter = 0;

        String TAG =  "RunTimerTask";

        @Override
        public void run() {
            Log.d(TAG, "stage="+stage + ", startdelay:" + startDelay + ", b_sendenable:" + b_sendenable);
            if (!b_sendenable) stage = 0;

            switch (stage) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    stage++;
                    break;
                case 11:
                    sendC0cmd();
                    iter_count = 0;
                    break;
                case 12:
                    switch (next_msg2send) {
                        case 0xc1:
                            sendC1cmd(u_englishMetric);
                            break;
                        case 0xc2:
                            if (iter_count == 0) {
                            /* write */
                                sendC2cmd(1, u_user, u_language, u_age, u_sex, u_weight, u_height);
                            }
                            else {
                            /* read */
                                sendC2cmd(0, 0, 0, 0, 0, 0, 0);
                            }
                            break;
                        case 0xc3:
                            if (iter_count < 5) {
                                sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte) 0, 0);
                            }
                            else {
                                if (prog_mode == 0) {
                                    /* manual mode */
                                    if (w_new_resistance_level_commanded) {
                                        cur_level = w_resistance_level;
                                        w_new_resistance_level_commanded = false;
                                        c3_cmd = 4;
                                        // cdubey : Added to change the resistance level
                                        sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte) 0, 0);
                                        c3_cmd = 0;
                                    }
                                    else {
                                        /* no change in resistance level (c3_cmd=0, 1=start, 2=stop */
                                        sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte)0, 0);
                                        c3_cmd = 0; /* added 2016-11-04 -- missing from iOS version */
                                    }
                                }
                                else {
                                    /* auto prog mode */
                                    if (w_new_resistance_level_commanded) {
                                        cur_level = w_resistance_level;
                                        w_new_resistance_level_commanded = false;
                                       // cdubey : Added to change the resistance level
                                        c3_cmd = 4;
                                        sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte) 0, 0);
                                        c3_cmd = 0;
                                    } else {
                                        /* no change in resistance level (c3_cmd=0, 1=start, 2=stop */
                                        sendC3cmd((byte) c3_cmd, (byte) cur_level, (byte) 0, 0);
                                        c3_cmd = 0; /* added 2016-11-04 */
                                    }
                                }
                            }
                            break;
                        case 0xc4:
                        /* set prog mode */
                        // cdubey : remove typecasting to byte
                            sendC4cmd((byte) u_program, (byte) u_mode, (byte) u_timeGoal,
                                     u_distanceGoal, u_calorieGoal,
                                     u_wattGoal, (byte) u_timeGoal);
                            break;
                        case 0xc5:
                        /* send program */
                            sendC5cmd((byte) 1, (byte) 1, (byte) 0x10,
                                    (byte) 2, (byte) 1, (byte) 2, (byte) 3,
                                    (byte) 4, (byte) 5, (byte) 6, (byte) 7,
                                    (byte) 8, (byte) 9, (byte) 10);
                            break;
                        case 0xc6:
                            break;
                    } /* switch next_msg2send */
                    break;
                case 13:
                    break;
            } /* switch */
        } /* public void run */



//        @Override
//        public void run() {
//            Log.d("mjp3", "tick_a" + b_connected + b_sendenable + ":" + startDelay);
//            rt_debugCounter += 10;
//            if (b_sendenable && (startDelay > 0)) {
//                startDelay--;
//Log.d("mjp3a", "delaying");
//            }
//            if ((b_sendenable) && (startDelay==0)) {
//                byte[] buf_send = new byte[20];
//                // old way E-Way
//                buf_send[0] = intTobyte(0xf2);
//                buf_send[1] = intTobyte(0xc0);
//                buf_send[2] = 0;
//                buf_send[3] = checkoutsum(buf_send);
//Log.d(TAG, "Sending: " + Integer.toHexString((buf_send[0] & 0xff)) + ":" +
//                           Integer.toHexString((buf_send[1] & 0xff)) + ":" +
//                           Integer.toHexString((buf_send[2] & 0xff)) + ":" +
//                           Integer.toHexString((buf_send[3] & 0xff)) + ":" +
//                           Integer.toHexString((buf_send[4] & 0xff)) + ":" +
//                           Integer.toHexString((buf_send[5] & 0xff)) + ":" +
//                           Integer.toHexString((buf_send[6] & 0xff)));
//
//                mUartService.writeRXCharacteristic(buf_send);
//            }
//        }
    } /* private class RunTimerTask */



    private class UARTStatusChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "UARTStatusChangeReceiver-onReceive");
            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                Log.d(TAG, "UARTStatusChangeReceiver-onReceive:ACTION_GATT_CONNECTED");
//                runOnUiThread(new Runable() {
//                    public void run() {
                    b_connected = true;
                    sendCommand = 0;
                    b_sendenable = true;
                    startDelay = 3;
//                });
            }

            if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                Log.d(TAG, "UARTStatusChangeReceiver-onReceive:ACTION_GATT_DISCONNECTED");
                b_connected = false;
                b_sendenable = false;
                /* b_sendenable = false; */ /* do we need this */
            }

            if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                Log.d(TAG, "UARTStatusChangeReceiver-onReceive:ACTION_GATT_SERVICES_DISCOVERED");
                mUartService.enableTXNotification();
            }

            if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {
                Log.d(TAG, "UARTStatusChangeReceiver-onReceive:ACTION_DATA_AVAILABLE");

                final byte[] bRecv = intent.getByteArrayExtra(UartService.EXTRA_DATA);
dumpRxBuffer(bRecv, (byte) bRecv.length);
                processRxBuffer(bRecv, (byte) bRecv.length);
            }

            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)) {
                Log.d(TAG, "UARTStatusChangeReceiver-onReceive:ACTION_NOT_SUPPORT_UART");
            }
        } /* UARTStatusChangeReceiver : onReceiver */

    } /* private class UARTStatusChangeReceiver */



    public static byte bytenumToASCII(byte byt) {
        int num = 0;
        byte ret = 0;
        num = byteToint(byt);
        num += 48;
        ret = intTobyte(num);
        return ret;
    }

    public static byte intTobyte(int data) {
        byte a = 0;
        a = (byte) (0xff & data);
        // a[1] = (byte) ((0xff00 & i) >> 8);
        // a[2] = (byte) ((0xff0000 & i) >> 16);
        // a[3] = (byte) ((0xff000000 & i) >> 24);
        return a;
    }

    public static int byteToint(byte byt) {
        int b = 0xff;
        b = b & (byt);
        return b;
    }

    public static String bytesToString(byte[] b, int length) {
        StringBuffer result = new StringBuffer("");
        for (int i = 0; i < length; i++) {
            result.append((char) (b[i]));
        }

        return new String(result);// result.toString();
    }

    public static byte checkoutsum(byte[] res) {
        byte byt = 0;
        int length = 0, sum = 0;
        length = res[2] + 3;
        for (int i = 0; i < length; i++) {
            sum += res[i];
        }
        byt = (byte) (sum & 0xff);
        return byt;
    }





    private void sendC0cmd() {

        byte len;
        byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc0, (byte) 0x00,
                (byte) 0xb2, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00 };

        len = buf[2];
        dumpTxBuffer(buf, len);
        mUartService.writeRXCharacteristic(buf);
    } /* sendC0cmd */

    private void sendC1cmd(int engMetric) {

        byte len;
        byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc1, (byte) 0x05,
                (byte) 0x01, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00 };

        buf[3] = (byte) ((engMetric != 0) ? 1 : 2); /* metric=1, english=2 */
        len = buf[2];
        byte cs = checkoutsum(buf);
        buf[2+len+1] = cs;
        dumpTxBuffer(buf, len);
        mUartService.writeRXCharacteristic(buf);
    } /* sendC1cmd */

    private void sendC2cmd(int rw, int user, int lang,
                           int age, int sex, int weight,
                           int height) {

        byte len;
        byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc2, (byte) 0x0d,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00 };

        if (rw != 0) {
            /* write */
            buf[3] = 1;
            buf[4] = (byte) user;
            buf[5] = (byte) lang;
            buf[6] = (byte) age;
            buf[7] = (byte) sex;
            //    weight xx.yy
            buf[8] = (byte) (weight >> 8);  // weight whole number xx // weight >> 8;
            buf[9] = (byte) (weight & 0xff);// weight fraction (yy) // weight & 0xff
            //    height xx.yy (maybe - unconfirmed
            buf[10] = (byte) (height >> 8); // height whole number xx // height >> 8;
            buf[11] = (byte) (height & 0xff);//height fraction (yy) // height & 0xff
        }
        else {
            buf[2] = 1; /* new length */
            buf[3] = 0;
        }

        byte cs = checkoutsum(buf);
        len = buf[2];
        buf[2+len+1] = cs;
        dumpTxBuffer(buf, len);
        mUartService.writeRXCharacteristic(buf);
    } /* sendC2cmd */

    private void sendC3cmd(byte keyValue, byte level, byte slope, int watt) {

        byte len;
        byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc3, (byte) 0x07,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00 };

        buf[3] = keyValue;
        buf[4] = level;
        buf[5] = slope;
        buf[6] = (byte) (watt / 256);
        buf[7] = (byte) (watt & 0xff);

        byte cs = checkoutsum(buf);
        len = buf[2];
        buf[2+len+1] = cs;
        dumpTxBuffer(buf, len);
        mUartService.writeRXCharacteristic(buf);
    } /* sendC3cmd */


    private void sendC4cmd(byte progNumber, byte mode, byte targetTime,
                           int distance, int calories, int watt,
                           byte targetHeart) {

        byte len;
        byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc4, (byte) 0x0d,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x00, (byte) 0x00 };

        buf[3] = mode;
        buf[4] = targetTime;
        buf[5] = (byte) (distance / 256);
        buf[6] = (byte) (distance & 0xff);
        buf[7] = (byte) (calories / 256);
        buf[8] = (byte) (calories & 0xff);
        buf[9] = (byte) (watt / 256);
        buf[10]= (byte) (watt & 0xff);
        buf[11]= targetHeart;
        buf[12]= progNumber;


        byte cs = checkoutsum(buf);
        len = buf[2];
        buf[2+len+1] = cs;
        dumpTxBuffer(buf, len);
        mUartService.writeRXCharacteristic(buf);
    } /* sendC4cmd */

    private void sendC5cmd(byte rw, byte dataProgram, byte numData,
                           byte progType,
                           byte data1, byte data2, byte data3, byte data4,
                           byte data5, byte data6, byte data7, byte data8,
                           byte data9, byte data10) {

        byte len;
        byte buf[] = new byte[]{(byte) 0xf2, (byte) 0xc5, (byte) 0x0d,
                                   (byte) 0x00, (byte) 0x00, (byte) 0x00,
                                   (byte) 0x00, (byte) 0x00, (byte) 0x00,
                                   (byte) 0x00, (byte) 0x00, (byte) 0x00,
                                   (byte) 0x00, (byte) 0x00, (byte) 0x00,
                                   (byte) 0x00, (byte) 0x00, (byte) 0x00,
                                   (byte) 0x00, (byte) 0x00 };
        if (rw == 0) {
            /* read */
            buf[3] = 0x00;
            buf[4] = dataProgram;
            buf[5] = numData;
            buf[6] = progType;
            len = 0x04;
        }
        else { /* write */
            buf[3] = 1;
            len = 0x0e;
            buf[4] = dataProgram;
            buf[5] = numData;
            buf[6] = progType;
            buf[7] = data1;
            buf[8] = data2;
            buf[9] = data3;
            buf[10]= data4;
            buf[11]= data5;
            buf[12]= data6;
            buf[13]= data7;
            buf[14]= data8;
            buf[15]= data9;
            buf[16]= data10;
        }
        buf[2] = len;
        byte cs = checkoutsum(buf);
        buf[2+len+1] = cs;
        dumpTxBuffer(buf, (byte) 20);
        mUartService.writeRXCharacteristic(buf);
    } /* sendC5cmd */


    private boolean validateChecksum(byte[] buf, byte len, byte expectedChecksum) {
        byte cs;
        boolean rval;
        int i;

        rval = true;
        cs = 0;
        for (i=0; i<len; i++) {
            cs += buf[i];
        }

        rval = cs == expectedChecksum;
        return rval;
    } /* validateChecksum */

    private int validateRxMessage(byte[] buf, byte len) {
        int rval;

        rval = -1;
        if (len >= 4) {
            if ((buf[0] == (byte) 0xf2) && (buf[1] >= (byte) 0xd0) && (buf[1] <= (byte) 0xdf)) {
                switch (buf[1]) {
                    case (byte) 0xd0:
                        if ((len == 4) && (buf[2] == 0)) {
                            /* validate checksum */
                            if (validateChecksum(buf, (byte) 3, buf[3])) {
                                rval = 1;
                            }
                        }
                        break;
                    case (byte) 0xd1:
                        if ((len == (0x0c + 4)) && (buf[2] == 0x0c)) {
                            /* validate checksum */
                            if (validateChecksum(buf, (byte) (0x0c+4-1), (byte) buf[0x0c+4-1])) {
                                rval = 1;
                            }
                        }
                        break;
                    case (byte) 0xd2:
                        if ((len == (0x0c+4)) && (buf[2] == 0x0c)) {
                            /* validate checksum */
                            if (validateChecksum(buf, (byte) (0x0c+4-1), buf[0x0c+4-1])) {
                                rval = 1;
                            }
                        }
                        break;
                    case (byte) 0xd3:
                        if ((len == (0x10+4)) && (buf[2] == 0x10)) {
                            /* validate checksum */
                            if (validateChecksum(buf, (byte) (0x10+4-1), buf[0x10+4-1])) {
                                rval = 1;
                            }
                        }
                        break;
                    case (byte) 0xd4:
                        if ((len == (0x02+4)) && (buf[2] == 0x02)) {
                            /* validate checksum */
                            if (validateChecksum(buf, (byte) (0x02+4-1), buf[0x02+4-1])) {
                                rval = 1;
                            }
                        }
                        break;
                    case (byte) 0xd5:
                        if ((len == (0x0d+4)) && (buf[2] == 0x0d)) {
                            /* validate checksum */
                            if (validateChecksum(buf, (byte) (0x0d+4-1), buf[0x0d+4-1])) {
                                rval = 1;
                            }
                        }
                        break;
                    case (byte) 0xd6:
                        break;
                    case (byte) 0xd7:
                        break;
                    case (byte) 0xd8:
                        break;
                    case (byte) 0xd9:
                        break;
                    case (byte) 0xda:
                        break;
                    default:
                        break;
                } /* switch */
            } /* if */
        } /* if len >= 4 */
        return rval;
    } /* validateRxMessage */

    private void dumpTxBuffer(byte[] buf, byte len) {
        StringBuilder sb = new StringBuilder();
        int i;
        for (i=0; i<len; i++) {
            sb.append(String.format("%02x ", buf[i]));
        }
        Log.d(TAG+"dumpTx"+i, sb.toString());
    } /* dumpTxBuffer */

    private void dumpRxBuffer(byte[] buf, byte len) {
        StringBuilder sb = new StringBuilder();
        int i;
        for (i=0; i<len; i++) {
            sb.append(String.format("%02x ", buf[i]));
        }
        Log.d("B=B dumpRx"+i, sb.toString());
    } /* dumpRxBuffer */


    private void processRxBuffer(byte [] buf, byte len) {
        int isMsgValid;
        isMsgValid = validateRxMessage(buf, len);

        //Log.d("prb", "IsMsgValid(:" + len + "):" + isMsgValid);
        if (isMsgValid < 0) Log.d("prb", "Invalud Message detected");

        if (isMsgValid > 0) {
            Log.d("B=B processRxBuffer", String.format("%02x", buf[1]));
            dumpRxBuffer(buf, len);
            switch (buf[1]) {
                case (byte) 0xd0:
                    iter_count = 0;
                    next_msg2send = 0xc1;
                    break;
                case (byte) 0xd1:
                    c_wattSupportEnabled = ((buf[3] != 0) ? 1 : 0);
                    c_hrcSupportEnabled = ((buf[4] != 0) ? 1 : 0);
                    c_maxLevel = (buf[5]);
                    c_maxSlope = (buf[6]);
                    c_metricEnglish = buf[7];
                    c_current_program = buf[8];
                    c_modelCoding1 = buf[12];
                    c_modelCoding2 = buf[13];
                    iter_count = 0;
                    next_msg2send = 0xc2;
                    break;
                case (byte) 0xd2:
                    if (buf[2] == 0xc2) {
                        c_current_user = buf[3];
                        c_current_language = buf[4];
                        c_current_age = buf[5];
                        c_current_sex = buf[6];
                        c_current_weightH = byteToint(buf[7]);
                        c_current_weightL = byteToint(buf[8]);
                        c_current_weight = (c_current_weightH * 256) + c_current_weightL;
                        c_current_heightH = byteToint(buf[9]);
                        c_current_heightL = byteToint(buf[10]);
                        c_current_height = (c_current_heightH * 256) + c_current_heightL;
                    }
                    iter_count++;
                    if (iter_count >= 2) {
                        if (prog_mode == 0) {
                            /* manual */
                            if ((u_distanceGoal == 0) &&
                                    (u_timeGoal == 0) &&
                                    (u_calorieGoal == 0)) {
                                next_msg2send = 0xc3;
                            }
                            else {
                                next_msg2send = 0xc4;
                            }
                        } else if (prog_mode == 1) {
                            next_msg2send = 0xc4; /* send parameters */
                        }
                    }
                    break;
                case (byte) 0xd3:
                    iter_count++;
                    c_current_minutes = buf[3];
                    c_current_seconds = buf[4];
                    c_current_distance1_0 = byteToint(buf[5]);
                    c_current_distance0_1 = byteToint(buf[6]);
                    c_current_distance = (c_current_distance1_0 * 256) + c_current_distance0_1;
                    c_current_caloriesH = byteToint(buf[7]);
                    c_current_caloriesL = byteToint(buf[8]);
                    c_current_calories = (c_current_caloriesH * 256) + c_current_caloriesL;
                    c_current_wattsH = byteToint(buf[9]);
                    c_current_wattsL = byteToint(buf[10]);
                    c_current_watts = (c_current_wattsH * 256) + c_current_wattsL;
                    c_current_heartrate = buf[11];
                    c_current_speedH = byteToint(buf[12]);
                    c_current_speedL = byteToint(buf[13]);
                    c_current_speed = (c_current_speedH * 256) + c_current_speedL;
                    c_current_slope = buf[14];
                    c_current_level = buf[15];
                    c_current_RPMx10_H = byteToint(buf[16]);
                    c_current_RPMx10_L = byteToint(buf[17]);
                    c_current_RPMx10 = (c_current_RPMx10_H * 256) + c_current_RPMx10_L;
                    c_current_status = buf[18];
                    Log.d("Rxd3", "Min:" + c_current_minutes + ":" + c_current_seconds + ",Distx100:" + c_current_distance + ",Cal:" + c_current_calories + ",Watts:" + c_current_watts + ",HR:" + c_current_heartrate + ",Spdx10:" + c_current_speed + ",Slp:" + c_current_slope + ",Lev:" + c_current_level + ",RPM:" + c_current_RPMx10 + ",Stat:" + c_current_status);
                    if ((req_stop) && ((c_current_status & 0x03) != 0)) {
                        if (c3_cmd == 0) {
                            c3_cmd = 2; /* second pause = stop */
                        }
                    }
                    break;
                case (byte) 0xd4:
                    c_current_program = buf[3];
                    c_current_mode = buf[4];
                    next_msg2send = 0xc3;
                    break;
                case (byte) 0xd5:
                    next_msg2send = 0xc3;
                    iter_count = 1000;
                    break;
                case (byte) 0xd6:
                    break;
                case (byte) 0xd7:
                    break;
                case (byte) 0xd8:
                    break;
                case (byte) 0xd9:
                    break;
                case (byte) 0xda:
                    break;
                case (byte) 0xdb:
                    break;
                case (byte) 0xdc:
                    break;
                case (byte) 0xdd:
                    break;
                case (byte) 0xde:
                    break;
                case (byte) 0xdf:
                    break;
                default:
                    break;
            } /* switch buf[1] */
        } /* if isMsgValid > 0 */
    } /* processRxBuffer */






    public int getConsoleMaxLevels() {
        return c_maxLevel;
    } /* getConsoleMaxLevels */

    public boolean isConsoleMetricMode() {
        return (c_metricEnglish != 0);
    } /* isConsoleMetricMode */

    public boolean isConsoleWattSupported() {
        return (c_wattSupportEnabled != 0);
    } /* isConsoleWattSupported */

    public boolean getConsoleProgram() {
        return (c_current_program != 0);
    } /* getConsoleProgram */

    public boolean isConsoleHRCSupport() {
        return (c_hrcSupportEnabled != 0);
    } /* isConsoleHRCSupport */

    public void setDisconnect() {
        stage = -1;
        /* do disconnect here !!!! ???? */
        /* ?????? */
        mUartService.disconnect(); /* does this work??? */
        Log.d(TAG, "mjp-setDisconnect");
    } /* setDisconnect */

    public boolean getIsConnected() {
        return (stage >= 10);
    } /* getIsConnected */

    public boolean getIsDisconnected() {
        return b_connected;
    } /* getIsDisconnected */

    public int getCurrentConsoleWorkoutRPM() {
        return ( c_current_RPMx10);
    } /* getCurrentConsoleWorkoutRPM */

    public int getCurrentConsoleWorkoutSpeed() {
        return c_current_speed;
    } /* getCurrentConsoleWorkoutSpeed */

    public int getCurrentConsoleWorkoutDistance() {
        return ( c_current_distance);
    } /* getCurrentConsoleWorkoutDistance */

    public int getCurrentConsleWorkoutCalories() {
        return (c_current_calories * 100);
    } /* getCurrentConsoleWorkoutCalories */

    public int getCurrentConsoleWorkoutProgram() {
        return c_current_program;
    } /* getCurrentConsoleWorkoutProgram */

    public int getCurrentConsoleWorkoutUser() {
        return c_current_user;
    } /* getCurrentConsoleWorkoutUser */

    public int getCurrentConsoleWorkoutTimeSeconds() {
        return ((c_current_minutes * 60) + c_current_seconds);
    } /* getCurrentConsoleWorkoutTimeSeconds */

    /* return 0:manual, 1:prog */
    public int getCurrentConsoleWorkoutMode() {
        return c_current_mode >> 2;
    } /* getCurrentConsoleWorkoutMode */

    /* return 0:stop, 1:run, 2:pause */
    public int getCurrentConsoleWorkoutStatus() {
        return (c_current_status & 0x03);
    } /* getCurrentConsoleWorkoutStatus */

    public int getCurrentConsoleWorkoutResistanceLevel() {
        return c_current_level;
    } /* getCurrentConsoleWorkoutResistanceLevel */

    public int getCurrentConsoleWorkoutPulse() {
        return c_current_heartrate;
    } /* getCurrentConsoleWorkoutPulse */

    /* ---------------------------------------------------------- */
    /* ---------------------------------------------------------- */
    public void setNewWorkoutResistanceLevel(int newLevel) {
        if (newLevel > c_current_level) {
            /* increase */
            w_new_resistance_level_commanded = true;
            w_resistance_level = newLevel;
        }
        else if (newLevel < c_current_level) {
            /* decrease */
            w_new_resistance_level_commanded = true;
            w_resistance_level = newLevel;
        }

    } /* setNewWorkoutResistanceLevel */

    public void setEnglishMetric(boolean EnglishMetric) {
        if (EnglishMetric) {
            u_englishMetric = 0;
        }
        else {
            u_englishMetric = 1;
        }
    } /* setEnglishMetric */

    public void setUserParameters(int userNumber,
                                  boolean userLanguageEnglish,
                                  int userAge,
                                  boolean userSexMale,
                                  int userHeight,
                                  int userWeight) {

        if (userNumber > 0) {
            u_user = userNumber;
        }
        else {
            u_user = 1;
        }

        if (userLanguageEnglish) {
            u_language = 0; /* english */
        }
        else {
            u_language = 0; /* english */
        }

        if (userAge > 0) {
            u_age = userAge;
        }
        else {
            u_age = 25; /* default */
        }

        if (userSexMale) {
            u_sex = 1; /* male */
        }
        else {
            u_sex = 0; /* female */
        }

        if (userHeight > 0) {
            u_height = userHeight;
        }
        else {
            u_height = (u_englishMetric == 0) ? 76 : ((int) (76/2.5)); /* english : metric */
        }

        if (userWeight > 0) {
            u_weight = userWeight;
        }
        else {
            u_weight = (u_englishMetric == 0) ? 100 : ((int) (100/2.2)); /* english : metric */
        }
    } /* setUserParameters */

    public void setUserWorkoutParameters(int modeAuto,
                                         int workoutProgramNumber,
                                         int workoutCalorieGoal,
                                         int workoutDistanceGoal,
                                         int workoutTimeGoal, /* minutes */
                                         int workoutTargetHRC) {

        u_calorieGoal = 0;
        u_wattGoal = 0;
        u_timeGoal = 0;
        u_distanceGoal = 0;

        if (modeAuto != 0) {
            u_mode = 1;
            prog_mode = 1;
            u_program = workoutProgramNumber;
        }
        else {
            u_mode = 0;
            u_program = 0; /* ?? */
            prog_mode = 0;
        }
        if (workoutTimeGoal > 0) {
            u_timeGoal = workoutTimeGoal;
        }
        else if (workoutCalorieGoal > 0) {
            u_calorieGoal = workoutCalorieGoal;
        }
        else if (workoutDistanceGoal > 0) {
            u_distanceGoal = workoutDistanceGoal;
        }
        stage = 8;
    } /* setUserWorkoutParameterModeAuto */

    public boolean setWorkoutStart() {

        Log.d(TAG,"********************** SETTING setWorkoutStart ");


        stage = 12;
        next_msg2send = 0xc1;
        iter_count = 0;
        req_stop = false;
        c3_cmd = 1;
        return true;
    } /* setWorkoutStart */

    public boolean setWorkoutPause() {
        c3_cmd = 2;
        req_stop = false;
        return true;
    } /* setWorkoutPause */

    public boolean setWorkoutStop() {
        if ((c_current_status & 0x03) == 1) {
            req_stop = true; // we are run mode --- first do a pause -then pause again=stop
        }
        c3_cmd = 2; /* second pause = stop */
        return true;
    } /* setWorkoutStop */

    public boolean setWorkoutResume() {
        c3_cmd = 1;
        req_stop = false;
        return true;
    } /* setWorkoutResume */

    public void setProgram(int newProg) {

        Log.d(TAG,"********************** SETTING setProgram "+newProg);


        u_calorieGoal = 0;
        u_wattGoal = 0;
        u_timeGoal = 0;
        u_distanceGoal = 0;

        if (newProg > 0) {
            u_mode = 1;
            prog_mode = 1;
            u_program = newProg;
        }
        else {
            u_mode = 0;
            prog_mode = 0;
            u_program = 0;
        }
    } /* setProgram */

    public void setGoalCal(int newGoal) {

        Log.d(TAG,"********************** SETTING setGoalCal "+newGoal);

        u_calorieGoal = newGoal;
        if (newGoal != 0) next_msg2send = 0xc4;
        stage = 8;
    } /* setGoalCal */

    public void setGoalDist(int newGoal) {

        u_distanceGoal = newGoal;

        u_distanceGoal *= 100;

        if (newGoal != 0) next_msg2send = 0xc4;
        stage = 8;
    } /* setGoalDist */

    public void setGoalTime(int newGoal) {

        Log.d(TAG,"********************** SETTING setGoalTime "+newGoal);


        u_timeGoal = newGoal;
        if (newGoal != 0) next_msg2send = 0xc4;
        stage = 8;
    } /* setGoalTime */



} /* public class MyActivity extends AppCompatActivity */


