package engineeringsoftwaresystems.com.prova14sss;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class AndroidbtmfqModuleActivity extends AppCompatActivity {

    public static final String TAG = "B=B AndroidbtmfqModule";

    private ControllerService mControllerService = null;

    private final int PERMISSION_ACCESS_FINE_LOCATION = 1;

    final static int REQUEST_LOCATION = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        Log.d(TAG, "-----onCreate.START");

        initServices(getApplicationContext(), this);

    }


    // Controller service connected/disconnected
    private ServiceConnection mServiceConnection = new ServiceConnection() {

        private static final String TAG = "B=B ServiceConnection";


        @Override
        public void onServiceConnected(ComponentName ClassName, IBinder rawBinder) {
            Log.d(TAG, "mServiceConnection.onServiceConnected()");
            mControllerService = ((ControllerService.LocalBinder) rawBinder).getService();

            if (mControllerService != null) {
                Log.d(TAG, "SCAN AND CONNECT");
                mControllerService.scanAndConnect();
            }


            Log.d(TAG, "mControllerService READY FOR API CALLING !!!=" + mControllerService);

        }

        @Override
        public void onServiceDisconnected(ComponentName classname) {
            Log.d(TAG, "mServiceConnection.onServiceDisconnected()");
            mControllerService = null;
        }
    };

    public void doButtonBT(View view) {

        Log.d(TAG, "doButtonBT.START");

        try {
            BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();

            if (bluetoothAdapter == null) {
                Log.d(TAG, "BT is not supported by your phone...");
                return;
            }

            if (bluetoothAdapter.isEnabled() == false) {
                bluetoothAdapter.enable();
                Log.d(TAG, "Enabling BT");
            } else if (bluetoothAdapter.isEnabled() == true) {
                bluetoothAdapter.disable();
                Log.d(TAG, "Disabling BT");
            }
        } catch (Exception e) {
            e.printStackTrace();

            Log.d(TAG, "doButtonBT.exception e=" + e.getMessage());
        }


        Log.d(TAG, "doButtonBT.END");
    }


    public void doButton1(View view) {

        Log.d(TAG, "doButton1.START");

        if (mControllerService != null) {
            mControllerService.scanAndConnect();
        }

        Log.d(TAG, "doButton1.END");
    }

    public void doButton2(View view) {
        Log.d(TAG, "doButton2.START mControllerService=" + mControllerService);


        if (mControllerService != null) {
            mControllerService.startWorkOut();
        }


        Log.d(TAG, "doButton2.END");
    }

    public void doButton3(View view) {

        Log.d(TAG, "doButton3.START mControllerService=" + mControllerService);

        if (mControllerService != null) {
            mControllerService.stopWorkout();
        }

        Log.d(TAG, "doButton3.END");

    }


    public void doButton_setProgam3(View view) {
        Log.d(TAG, "doButton_setProgam.START vale==" + 3);


        if (mControllerService != null) {
            mControllerService.setProgram(3);
        }


        Log.d(TAG, "doButton_setProgam.END");
    }


    public void doButton_setProgam4(View view) {
        Log.d(TAG, "doButton_setProgam.START vale==" + 4);


        if (mControllerService != null) {
            mControllerService.setProgram(4);
        }


        Log.d(TAG, "doButton_setProgam.END");
    }


    public void doButton_setProgam5(View view) {
        Log.d(TAG, "doButton_setProgam.START vale==" + 5);


        if (mControllerService != null) {
            mControllerService.setProgram(5);
        }


        Log.d(TAG, "doButton_setProgam.END");
    }


    public void doButton_SetGoalCal100(View view) {
        Log.d(TAG, "SetGoalCal.START");

        int cal = 100;

        Log.d(TAG, "cal==" + cal);

        if (mControllerService != null) {
            mControllerService.setGoalCal(cal);
        }

        Log.d(TAG, "doButton_setProgam.END");
    }

    public void doButton_SetGoalCal200(View view) {
        Log.d(TAG, "SetGoalCal.START");

        int cal = 200;


        Log.d(TAG, "cal==" + cal);

        if (mControllerService != null) {
            mControllerService.setGoalCal(cal);
        }

        Log.d(TAG, "doButton_setProgam.END");
    }

    public void doButton_SetGoalCal350(View view) {
        Log.d(TAG, "SetGoalCal.START");

        int cal = 350;

        Log.d(TAG, "cal==" + cal);

        if (mControllerService != null) {
            mControllerService.setGoalCal(cal);
        }

        Log.d(TAG, "doButton_setProgam.END");
    }


    public void doButton_SetGoalCal700(View view) {
        Log.d(TAG, "SetGoalCal.START");

        int cal = 700;

        Log.d(TAG, "cal==" + cal);

        if (mControllerService != null) {
            mControllerService.setGoalCal(cal);
        }

        Log.d(TAG, "doButton_setProgam.END");
    }


    public void doButton_setGoalDist10(View view) {
        Log.d(TAG, "setGoalDist.START");

        if (mControllerService != null) {

            int dist = 10;

            Log.d(TAG, "dist" + dist);
            //mControllerService.setEnglishMetric(true);// this function works the opposite way
            mControllerService.setGoalDist(dist);
        }
        Log.d(TAG, "setGoalDist.END");
    }

    public void doButton_setGoalDist20(View view) {
        Log.d(TAG, "setGoalDist.START");

        if (mControllerService != null) {

            int dist = 20;

            Log.d(TAG, "dist" + dist);
            // mControllerService.setEnglishMetric(true);// this function works the opposite way
            mControllerService.setGoalDist(dist);
        }
        Log.d(TAG, "setGoalDist.END");
    }


    public void doButton_setGoalDist30(View view) {
        Log.d(TAG, "setGoalDist.START");

        if (mControllerService != null) {

            int dist = 30;

            Log.d(TAG, "dist" + dist);
            // mControllerService.setEnglishMetric(true);// this function works the opposite way
            mControllerService.setGoalDist(dist);
        }
        Log.d(TAG, "setGoalDist.END");
    }


    public void doButton_setGoalTime(View view) {
        Log.d(TAG, "setGoalTime.START mControllerService=" + mControllerService);


        if (mControllerService != null) {
            mControllerService.setGoalTime(45);
        }


        Log.d(TAG, "setGoalTime.END");
    }


    public void doButton_setWorkStartStart(View view) {
        Log.d(TAG, "setWorkStartStart.START mControllerService=" + mControllerService);


        if (mControllerService != null) {
            mControllerService.setWorkoutStart();
        }


        Log.d(TAG, "doButton2.END");
    }

    public void set_WorkoutResistanceLevel(View view) {

        if (mControllerService != null) {

            int workoutResistanceLevel = 7;

            Log.d(TAG, "workoutResistanceLevel=" + workoutResistanceLevel);

            mControllerService.setNewWorkoutResistanceLevel(workoutResistanceLevel);

        }


        //  Log.d(TAG, "set_WorkoutResistanceLevelIncrease.END");
    }


    public void set_WorkoutResistanceLevelIncrease(View view) {
        //  Log.d(TAG, "set_WorkoutResistanceLevelIncrease.START");

        if (mControllerService != null) {

            int workoutResistanceLevel = mControllerService.getCurrentConsoleWorkoutResistanceLevel();

            Log.d(TAG, "CURRENT workoutResistanceLevel=" + workoutResistanceLevel);

            workoutResistanceLevel = workoutResistanceLevel + 1;


            mControllerService.setNewWorkoutResistanceLevel(workoutResistanceLevel);

            Log.d(TAG, "NEW  workoutResistanceLevel=" + mControllerService.getCurrentConsoleWorkoutResistanceLevel());


        }


        //  Log.d(TAG, "set_WorkoutResistanceLevelIncrease.END");
    }


    public void set_WorkoutResistanceLevelDecrease(View view) {
        // Log.d(TAG, "set_WorkoutResistanceLevelDecrease.START");

        if (mControllerService != null) {
            int workoutResistanceLevel = mControllerService.getCurrentConsoleWorkoutResistanceLevel();

            Log.d(TAG, "CURRENT workoutResistanceLevel=" + workoutResistanceLevel);

            workoutResistanceLevel = workoutResistanceLevel - 1;


            mControllerService.setNewWorkoutResistanceLevel(workoutResistanceLevel);

            Log.d(TAG, "NEW  workoutResistanceLevel=" + mControllerService.getCurrentConsoleWorkoutResistanceLevel());
            //     mControllerService.setNewWorkoutResistanceLevel(workoutResistanceLevel);

        }


        //  Log.d(TAG, "set_WorkoutResistanceLevelDecrease.END");
    }

    public void initServices(Context context, Activity activity) {
        Log.d(TAG, "initServices.start");


        IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBTReceiver, filter1);


        IntentFilter filter2 = new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
        registerReceiver(mLocReceiver, filter2);


        if (!hasAllPermissions(context, activity)) {
            Log.d(TAG, "Requesting permissions before proceeding...");
            return;
        }

        BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();

        if (bluetoothAdapter == null) {
            Log.d(TAG, "BT is not supported by your phone...");
            return;
        }

        if (bluetoothAdapter.isEnabled() == false) {
            bluetoothAdapter.enable();
            Log.d(TAG, "Enabling BT before proceeding...");
            return;
        }


        boolean location_enabled = isLocationEnabled(this);
        if (!location_enabled) {
            Log.d(TAG, "Request for manually enabling the location services");
            buildAlertMessageNoGps(this);//pass activity
            return;
        }


        /* A Started Service will remain in memory until stoped manually */
        Intent startIntent = new Intent(activity, ControllerService.class);
        context.startService(startIntent);

        Intent bindIntent = new Intent(this, ControllerService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        Log.d(TAG, "initServices.end");
    }

    /*
   The method below will only execute if the SDK version is > M.
    */
    public boolean hasAllPermissions(Context context, Activity activity) {

        boolean hasAllPermissions = true;

        Log.d(TAG, "hasAllPermissions.START");

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int permissionCheck = context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");

                Log.d(TAG, "PERMISSION_ACCESS_FINE_LOCATION permissionCheck=" + permissionCheck);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    hasAllPermissions = false;
                    activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_FINE_LOCATION);
                }
            } else {
                Log.d(TAG, "hasAllPermissions: No need to check permissions. SDK version < LOLLIPOP.");
            }
        } catch (Exception e) {
            Log.e(TAG, "hasAllPermissions.Exception e" + e.getMessage());
        }

        return hasAllPermissions;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        Log.d(TAG, "onRequestPermissionsResult.START");
        Log.d(TAG, "requestCode=" + requestCode);

        int grant = PackageManager.PERMISSION_DENIED;

        if (grantResults != null && grantResults.length > 0) {
            grant = grantResults[0];
        }

        Log.d(TAG, "grant=" + grant);
        switch (requestCode) {
            case PERMISSION_ACCESS_FINE_LOCATION:
                if (grant == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "PERMISSION_ACCESS_FINE_LOCATION granted");
                    initServices(getApplicationContext(), this);
                }
                break;

            default:
                break;
        }

        Log.d(TAG, "onRequestPermissionsResult.END");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy.start");

        stopServices();

        Log.d(TAG, "onDestroy.stop");

    }

    public BluetoothAdapter getBluetoothAdapter() {
        Log.d(TAG, "getBluetoothAdapter.start");

        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

        BluetoothAdapter bluetoothAdapter = null;

        Log.d(TAG, "bluetoothManager=" + bluetoothManager);

        if (bluetoothManager != null) {
            bluetoothAdapter = bluetoothManager.getAdapter();
        }

        Log.d(TAG, "bluetoothAdapter =" + bluetoothAdapter);

        if (bluetoothAdapter == null) {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }

        Log.d(TAG, "getBluetoothAdapter.end bluetoothAdapter=" + bluetoothAdapter);
        return bluetoothAdapter;
    }


    private final BroadcastReceiver mBTReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            Log.d(TAG, "BroadcastReceiver onReceive action=" + action);


            try {
                if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                    final int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                            BluetoothAdapter.ERROR);

                    Log.d(TAG, "BroadcastReceiver onReceive bluetoothState=" + bluetoothState);


                    switch (bluetoothState) {
                        case BluetoothAdapter.STATE_ON:
                            Log.d(TAG, "BluetoothAdapter.STATE_ON : BT is now ON , calling initServices");
                            initServices(getApplicationContext(), AndroidbtmfqModuleActivity.this);
                            break;

                        case BluetoothAdapter.STATE_OFF:
                            Log.d(TAG, "BluetoothAdapter.STATE_OFF");
                            stopServices();
                            break;

                    }
                }
            } catch (Exception e) {
                e.getMessage();
                Log.d(TAG, "BroadcastReceiver.onReceive e=" + e.getMessage());
            }
        }
    };

    private final BroadcastReceiver mLocReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            Log.d(TAG, "BroadcastReceiver mLocReceiver onReceive action=" + action);

            try {

                if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                    Log.i(TAG, "Location Providers changed");

                    boolean isGpsEnabled;
                    boolean isNetworkEnabled;

                    LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                    isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                    if (isGpsEnabled || isNetworkEnabled) {
                        Log.d(TAG, "Location SERVICES ON");
                        initServices(getApplicationContext(), AndroidbtmfqModuleActivity.this);
                    } else {
                        Log.d(TAG, "Location SERVICES OFF");
                        stopServices();
                    }
                }

            } catch (Exception e) {
                e.getMessage();
                Log.d(TAG, "BroadcastReceiver.onReceive e=" + e.getMessage());
            }
        }
    };

    private boolean isLocationEnabled(Context context) {
        boolean location_enabled = false;
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        try {
            location_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

            Log.d(TAG, "Location Is Enabled ? " + location_enabled);

        } catch (Exception ex) {
            Log.d(TAG, "Location Exception" + ex.getMessage());
        }

        return location_enabled;
    }

    private void buildAlertMessageNoGpsDialog(Activity activity) {

        Log.d(TAG,"buildAlertMessageNoGpsDialog.START");
        try {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Location services are disabled, do you want to enable the settings?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

        } catch (Exception e) {

            Log.d(TAG,"buildAlertMessageNoGpsDialog.excepion e="+e.getMessage());
            e.printStackTrace();
        }
        Log.d(TAG,"buildAlertMessageNoGpsDialog.END");
    }

    private void buildAlertMessageNoGps(Activity activity) {

        Log.d(TAG,"buildAlertMessageNoGps.START");
        try {
            Intent intent = new Intent();

            intent.setClass(activity,LocationDialogActivity.class);

            activity.startActivityForResult(intent,REQUEST_LOCATION);
        } catch (Exception e) {

            Log.d(TAG,"buildAlertMessageNoGps.excepion e="+e.getMessage());
            e.printStackTrace();
        }
        Log.d(TAG,"buildAlertMessageNoGps.END");
    }



    public void stopServices() {
        Log.d(TAG, "stopServices.start STOP_ALL");

        try {
            unbindService(mServiceConnection);

        } catch (Exception e) {
            Log.d(TAG, "stopServices.exception 2 e=" + e.getMessage());
        }


        try {
            mControllerService.stopServices();

        } catch (Exception e) {
            Log.d(TAG, "stopServices.exception 4 e=" + e.getMessage());
        }


        try {
            Intent intent = new Intent(getApplicationContext(), ControllerService.class);
            stopService(intent);

        } catch (Exception e) {
            Log.d(TAG, "stopServices.exception 1 e=" + e.getMessage());
        }


        Log.d(TAG, "stopServices.end");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_LOCATION && resultCode == RESULT_OK)
        {
           startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }

    @Override
    public void onBackPressed() {

        Log.d(TAG, "onBackPressed.start");

        super.onBackPressed();

        stopServices();

        Log.d(TAG, "onBackPressed.end");

    }


}
