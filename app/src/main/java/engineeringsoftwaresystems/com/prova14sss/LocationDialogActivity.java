package engineeringsoftwaresystems.com.prova14sss;

/**
 * Created by DELL on 11/16/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class LocationDialogActivity  extends Activity implements OnClickListener {

    public final String TAG = "B=B "+getClass();


    Button ok_btn, cancel_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        Log.d(TAG,"onCreate.START");

        setContentView(R.layout.location_dialog);

        ok_btn = (Button) findViewById(R.id.ok_btn_id);
        cancel_btn = (Button) findViewById(R.id.cancel_btn_id);

        ok_btn.setOnClickListener(this);
        cancel_btn.setOnClickListener(this);

        this.setFinishOnTouchOutside(false);

        Log.d(TAG,"onCreate.END");

    }

    @Override
    public void onBackPressed() {

        Log.d(TAG, "onBackPressed.DO_NOTHING");
    }
    @Override
    public void onClick(View v) {

        Log.d(TAG,"onClick.START");

        Intent intent = new Intent();

        switch (v.getId()) {
            case R.id.ok_btn_id:

                Log.d(TAG,"onClick.RESULT_OK");


                setResult(RESULT_OK,intent);
                this.finish();
                break;

            case R.id.cancel_btn_id:
                Log.d(TAG,"onClick.RESULT_CANCELED");
                setResult(RESULT_CANCELED,intent);
                this.finish();
                break;
        }

        Log.d(TAG,"onClick.END");
    }
}